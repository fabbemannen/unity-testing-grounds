﻿using UnityEngine;
using System.Collections;

public class Destroyer : MonoBehaviour {

	public bool RespawnPlayer = false;
	public GameObject RespawnPoint;

	void OnTriggerEnter(Collider other)
	{
		if((other.tag == "Player" && RespawnPlayer == true) || other.tag != "")
		{
			other.transform.position = RespawnPoint.transform.position;
		}
		else
		{
			Destroy(other.gameObject);
		}
	}
}
