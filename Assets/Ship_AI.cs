﻿using UnityEngine;
using System.Collections;

public class Ship_AI : MonoBehaviour {
	public float speed = 50.0f;
	public float maxVelocity = 8.0f;
	public GameObject target;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		rigidbody.AddRelativeForce(Vector3.back * speed * Time.deltaTime);
		if(rigidbody.velocity.sqrMagnitude > maxVelocity)
		{
			rigidbody.velocity *= 0.9f;
		}

		if(rigidbody.velocity.sqrMagnitude > 700.0f && Vector3.Distance(target.transform.position, gameObject.transform.position) < 50.0f)
		{
			rigidbody.velocity *= 0.8f;
		}
		//Debug.Log (string.Format("sqrMagnitude: {0}", rigidbody.velocity.sqrMagnitude));
		//Debug.Log (string.Format("Distance to CP: {0}", Vector3.Distance(target.transform.position, gameObject.transform.position)));
	}

	void OnTriggerEnter(Collider other)
	{
		Debug.Log (string.Format("Collision between {0} and {1}", other.gameObject.name, gameObject.name));
		Vector3 expPosition = ((other.transform.position - transform.position) * 0.5f) + transform.position;
		rigidbody.AddExplosionForce(1000.0f, expPosition, 100.0f, 0.0f, ForceMode.Impulse);
	}
}
