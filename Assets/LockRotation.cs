﻿using UnityEngine;
using System.Collections;

public class LockRotation : MonoBehaviour {
	Quaternion InitialRotation;
	public bool lock_x = true;
	public bool lock_y = true;
	public bool lock_z = true;
	public bool lock_w = true;

	// Use this for initialization
	void Start () {
		InitialRotation = transform.rotation;
	}
	
	// Update is called once per frame
	void LateUpdate () {
		if(lock_x && lock_y && lock_z && lock_w)
		{
			transform.rotation = InitialRotation;
		}
		else
		{
			transform.rotation.Set(CheckRotation(0), CheckRotation(1), CheckRotation(2), CheckRotation(3)); //DOES NOT WORK, ALL LOCKED OR FIX IT 
		}
	}

	float CheckRotation (int direction)
	{
		if(direction == 0)
		{
			if(lock_x)
			{
				return InitialRotation.x;
			}
			else
			{
				return transform.rotation.x;
			}
		}
		else if(direction == 1)
		{
			if(lock_y)
			{
				return InitialRotation.y;
			}
			else
			{
				return transform.rotation.y;
			}
		}
		else if(direction == 2)
		{
			if(lock_z)
			{
				return InitialRotation.z;
			}
			else
			{
				return transform.rotation.z;
			}
		}
		else
		{
			if(lock_w)
			{
				return InitialRotation.w;
			}
			else
			{
				return transform.rotation.w;
			}
		}
	}
}
