﻿using UnityEngine;
using System.Collections;

public class GUI_Position : MonoBehaviour {
	public enum CustomList 
	{
		BottomLeft, BottomRight, Center, TopLeft, TopRight
	}
	public CustomList StartingFrom;

	public float x = 0;
	public float y = 0;

	private GUITexture guitexture;

	// Use this for initialization
	void Start () {
		guitexture = gameObject.GetComponent("GUITexture") as GUITexture;
		float gwidth = guiTexture.texture.width;
		float gheight = guiTexture.texture.height;
		
		//guitexture.pixelInset = new Rect(x - Screen.width / 2, y - Screen.height / 2, guiTexture.texture.width, guiTexture.texture.height);
		switch ((int)StartingFrom)
		{
		case 0: //Bottom Left
			guitexture.pixelInset = new Rect(x, y, gwidth, gheight);
			break;
		case 1: //Bottom Right
			guitexture.pixelInset = new Rect(Screen.width - gwidth - x, y, gwidth, gheight);
			break;
		case 2: //Center
			guitexture.pixelInset = new Rect((Screen.width / 2) - (gwidth / 2) - x, (Screen.height / 2) - (gheight / 2) - y, gwidth, gheight);
			break;
		case 3: //Top Left
			guitexture.pixelInset = new Rect(x, Screen.height - gheight - y, gwidth, gheight);
				break;
		case 4: //Top Right
			guitexture.pixelInset = new Rect(Screen.width - gwidth - x, Screen.height - gheight - y, gwidth, gheight);
			break;
		default:
			guitexture.pixelInset = new Rect(x, y, gwidth, gheight);
			break;
		}
	}
	
	// Update is called once per frame
	void Update () {
	}
}
