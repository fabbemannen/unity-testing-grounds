﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour {

	public GameObject splash_effect;
	private Vector3 InitialPosition;

	// Use this for initialization
	void Start () {
		InitialPosition = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		float distance = Vector3.Distance(InitialPosition, transform.position);
		if(distance > 150.0f)
		{
			if(transform.childCount > 0)
			{
				if(transform.GetChild(0).transform.particleEmitter != null)
				{
					transform.GetChild(0).transform.particleEmitter.emit = false;
				}
				else if(transform.particleEmitter != null)
				{
					transform.particleEmitter.emit = false;
				}
			}
			StartCoroutine(Wait(3.0f));
		}

	}
	
	void OnTriggerEnter(Collider collision)
	{
		if(collision.tag != "Player")
		{
			Vector3 explosionPos = transform.position;
			Collider[] colliders = Physics.OverlapSphere(explosionPos, 5.0f);
			foreach (Collider hit in colliders) {
				if (hit && hit.rigidbody)
					hit.rigidbody.AddExplosionForce(100.0f, explosionPos, 0.0f, -0.8F);
				
			}
			/*
			if(collision.rigidbody != null)
			{
				collision.rigidbody.AddExplosionForce(100.0f, transform.position, 100.0f, 0.0f);
			}
			
	*/
			GameObject clone = Instantiate(splash_effect, transform.position, transform.rotation) as GameObject;
			transform.rigidbody.velocity = new Vector3(0.0f, 0.0f, 0.0f);
			if(transform.childCount > 0)
			{
				if(transform.GetChild(0).transform.particleEmitter != null)
				{
					transform.GetChild(0).transform.particleEmitter.emit = false;
				}
				else if(transform.particleEmitter != null)
				{
					transform.particleEmitter.emit = false;
				}
			}
			transform.collider.enabled = false;
			StartCoroutine(Wait(3.0f));
		}
	}

	IEnumerator Wait(float seconds)	
	{
		yield return new WaitForSeconds(seconds);
		Destroy(gameObject);
	}
}
