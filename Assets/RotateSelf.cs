﻿using UnityEngine;
using System.Collections;

public class RotateSelf : MonoBehaviour {

	public float x;
	public float y;
	public float z;
	public float speed;
	public float max_speed; //Leave at zero if not random
	public bool random_direction = false;

	// Use this for initialization
	void Start () {
		if(max_speed != 0)
		{
			speed = Random.Range(speed, max_speed);
		}
		if(random_direction == true)
		{
			if(Random.Range(0, 10) < 5)
			{
				x *= -1;
				y *= -1;
				z *= -1;
			}
		}
		iTween.RotateBy(gameObject, iTween.Hash("x", x, "y", y, "z", z, "easeType", "linear", "loopType", "Loop", "speed", speed));
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
