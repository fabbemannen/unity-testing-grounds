﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class spawn_collision : MonoBehaviour {

	public List<GameObject> Objects = new List<GameObject>();
	GameObject go;

	// Use this for initialization
	void Start () {
		go = gameObject;
	}

	void OnCollisionEnter(Collision other)
	{
		int r = Random.Range(0, Objects.Count);
		Vector3 newPos = new Vector3(gameObject.transform.position.x, gameObject.transform.position.y, gameObject.transform.position.z);
		GameObject clone = Instantiate(Objects[r], newPos, other.transform.rotation) as GameObject;
		//clone.particleEmitter.enabled = false;
		if(clone.collider != null && go.collider != null)
		{
			Physics.IgnoreCollision(clone.collider, go.collider);
		}
	}
}
