﻿using UnityEngine;
using System.Collections;

public class Shooting : MonoBehaviour {

	//Should be remade to add projectile types through a dynamic list instead

	//Type 0: One projectile (e.g. grenade)
	//Type 1: On/off spawner (e.g. minigun)

	//0
	public GameObject ShotNormal;
	public int ShotNormal_type = 0;
	public float ShotNormal_speed;
	public float ShotNormal_fireRate = 0.5F;
	//1
	public GameObject ShotGrenade;
	public int ShotGrenade_type = 0;
	public float ShotGrenade_speed;
	public float ShotGrenade_fireRate = 0.5F;
	//2
	public GameObject ShotCluster;
	public int ShotCluster_type = 0;
	public float ShotCluster_speed;
	public float ShotCluster_fireRate = 0.5F;
	//3
	public GameObject ShotMinigun;
	public int ShotMinigun_type = 1;
	public float ShotMinigun_speed;
	public float ShotMinigun_fireRate = 0.5F;

	private float fireRate = 0.5F;
	private float nextFire = 0.0F;
	private GameObject go;
	private float go_speed;
	private float go_type = 0;

	private GameObject temp_child;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update ()
	{
		if(Input.GetButton("Fire1") && (Input.GetAxis("Mouse ScrollWheel") > 0 || Input.GetAxis("Mouse ScrollWheel") < 0) && temp_child != null)
		{
			temp_child = null;
		}

		if(Input.GetAxis("Mouse ScrollWheel") > 0)
		{
			if(Variables.weapon_active < Variables.weapon_slots - 1)
			{
				Variables.weapon_active++;
			}
			else
			{
				Variables.weapon_active = 0;
			}
		}
		else if(Input.GetAxis("Mouse ScrollWheel") < 0)
		{
			if(Variables.weapon_active > 0)
			{
				Variables.weapon_active--;
			}
			else
			{
				Variables.weapon_active = Variables.weapon_slots - 1;
			}
		}


		switch (Variables.weapon_active)
		{
		case 0:
			go = ShotNormal;
			go_speed = ShotNormal_speed;
			fireRate = ShotNormal_fireRate;
			break;
		case 1:
			go = ShotGrenade;
			go_speed = ShotGrenade_speed;
			fireRate = ShotGrenade_fireRate;
			break;
		case 2:
			go = ShotCluster;
			go_speed = ShotCluster_speed;
			fireRate = ShotCluster_fireRate;
			break;
		case 3:
			go = ShotMinigun;
			go_speed = ShotMinigun_speed;
			fireRate = ShotMinigun_fireRate;
			go_type = ShotMinigun_type;
			break;
		}

		if (Input.GetButton("Fire1") && Time.time > nextFire && go_type == 0)
		{
			nextFire = Time.time + fireRate;
			GameObject projectile = Instantiate(go, new Vector3(transform.position.x, transform.position.y + 1.0f, transform.position.z), 
			                                    transform.rotation) as GameObject;
			Physics.IgnoreCollision(collider, projectile.collider);
			if(projectile.rigidbody != null)
			{
				projectile.rigidbody.AddForce(transform.GetChild(1).transform.TransformDirection (Vector3.forward * go_speed * Time.deltaTime));
			}
		}
		else if(Input.GetButton("Fire1") && go_type == 1 && temp_child == null)
		{
			GameObject projectile = Instantiate(go, new Vector3(transform.position.x, transform.position.y + 1.0f, transform.position.z), 
			                                    transform.rotation) as GameObject;
			projectile.transform.parent = gameObject.transform;
			projectile.tag = "Projectile_Spawner";
			temp_child = projectile;
			//projectile.SendMessage("SetFireRate", fireRate, SendMessageOptions.RequireReceiver);
			//projectile.SendMessage("SetSpeed", go_speed, SendMessageOptions.RequireReceiver);
			//projectile.SetFireRate(fireRate);
			//projectile.SetSpeed(go_speed);
			//projectile.GetComponent<Spawn_Object>().SetFireRate(fireRate);
			//projectile.GetComponent<Spawn_Object>().SetSpeed(go_speed);
		}
	}
}
