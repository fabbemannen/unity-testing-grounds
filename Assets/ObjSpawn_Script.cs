﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ObjSpawn_Script : MonoBehaviour {
	
	public List<GameObject> Objects = new List<GameObject>();
	public GameObject ObjSpawner;
	public float spawnRate = 0.2F;
	private float nextSpawn = 0.0F;

	// Use this for initialization
	void Start () {
		renderer.material.color = Color.grey;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerStay(Collider collision)
	{
		//Debug.Log("Collided with " + collision.tag);
		if (collision.tag == "Player")
		{
			renderer.material.color = Color.green;
			if(Time.time > nextSpawn)
			{
				int r = Random.Range(0, Objects.Count);
				if(Random.Range(0, 100) > 0 && r == 1)
				{
					r = 0;
				}
				else if(Random.Range(0, 100) > 25 && r == 2)
				{
					r = 0;
				}
				nextSpawn = Time.time + spawnRate;
				GameObject clone = Instantiate(Objects[r], new Vector3(ObjSpawner.transform.position.x + Random.Range(-2, 2), 
				                                                       ObjSpawner.transform.position.y, 
				                                                       ObjSpawner.transform.position.z + Random.Range(-2, 2)), 
				                                                       ObjSpawner.transform.rotation) as GameObject;
				Color c = new Color(Random.Range(0.0f, 1.0f),Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f));
				clone.renderer.material.color = c;
			}
		}
	}

	void OnTriggerExit(Collider collision)
	{
		if (collision.tag == "Player")
		{
			renderer.material.color = Color.grey;
		}
	}
}
