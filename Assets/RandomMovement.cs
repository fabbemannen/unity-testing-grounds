﻿using UnityEngine;
using System.Collections;

public class RandomMovement : MonoBehaviour {
	public float minTime = 0.5F;
	public float maxTime = 0.5F;
	private float rateTime = 2.5f;
	private float nextTime = 0.0F;
	public GameObject target;
	public bool targetEnabled = false;


	// Use this for initialization
	void Start () {
		if(target == null && targetEnabled == true)
		{
			target = GameObject.FindGameObjectWithTag("Player");
		}

		rateTime = Random.Range(minTime, maxTime);
	}
	
	// Update is called once per frame
	void Update () {
		if(Time.time > nextTime)
		{
			nextTime = Time.time + rateTime;
			rateTime = Random.Range(minTime, maxTime);
			rigidbody.AddForce(new Vector3(Random.Range(-200, 200), Random.Range(-200, 200), Random.Range(-200, 200)));

			if(target != null)
			{
				if(transform.position.y < target.transform.position.y + 5.0f)
				{
					rigidbody.AddForce(new Vector3(0.0f, 500.0f, 0.0f));
				}
				float dist = Vector3.Distance(target.transform.position, transform.position);
				if(dist > 25.0f)
				{
					//Debug.Log ("Distance: " + dist);
					rigidbody.AddForce(transform.TransformDirection (Vector3.forward * 30000.0f * Time.deltaTime));
					//float step = 100.0f * Time.deltaTime;
					//transform.position = Vector3.MoveTowards(transform.position, target.transform.position, step);
				}
			}
		}
	}
}
