﻿using UnityEngine;
using System.Collections;

public class Position_Random : MonoBehaviour {

	public bool local_position = true;
	public float min_x = 0.0f;
	public float max_x = 0.0f;
	public float min_y = 0.0f;
	public float max_y = 0.0f;
	public float min_z = 0.0f;
	public float max_z = 0.0f;
	private GameObject go;

	// Use this for initialization
	void Start () {
		if(local_position = true)
		{
			gameObject.transform.localPosition = new Vector3(Random.Range(min_x, max_x), Random.Range(min_y, max_y), Random.Range(min_z, max_z));
		}
		else
		{
			gameObject.transform.position = new Vector3(Random.Range(min_x, max_x), Random.Range(min_y, max_y), Random.Range(min_z, max_z));
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void NewPos()
	{
		Start(); //haha
	}
}
