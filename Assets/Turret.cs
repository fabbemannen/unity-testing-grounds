﻿using UnityEngine;
using System.Collections;

public class Turret : MonoBehaviour {

	public GameObject projectile;
	public float speed;
	public float fireRate = 2.5F;
	private float nextFire = 0.0F;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		if (Time.time > nextFire)
		{
			nextFire = Time.time + fireRate;
			GameObject clone = Instantiate(projectile, new Vector3(transform.position.x, transform.position.y, transform.position.z), 
			                                    transform.rotation) as GameObject;
			Physics.IgnoreCollision(collider, clone.collider);
			clone.rigidbody.AddForce(transform.TransformDirection (Vector3.forward * speed * Time.deltaTime));
		}
	}
}
