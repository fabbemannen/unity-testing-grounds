﻿using UnityEngine;
using System.Collections;

public class RandomColor : MonoBehaviour {
	public bool disco = false;
	public bool light = false;
	public bool disco_light = false;
	// Use this for initialization
	void Start () {
		Color c = new Color(Random.Range(0.0f, 1.0f),Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f));
		if(gameObject.renderer != null)
		{
			renderer.material.color = c;
		}
		if(light == true && gameObject.light != null)
		{
			gameObject.light.color = c;
		}
	}
	
	// Update is called once per frame
	void Update () {
		Color c = new Color(Random.Range(0.0f, 1.0f),Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f));
		if(disco == true && gameObject.renderer != null)
		{
			renderer.material.color = c;
		}
		if(disco_light == true && gameObject.light != null)
		{
			gameObject.light.color = c;
		}
	}
}
