﻿using UnityEngine;
using System.Collections;

public class Camera_Underwater : MonoBehaviour {

	//Attach to camera
	public GameObject water;
	private bool defaultFog ;
	private Color defaultFogColor;
	private float defaultFogDensity;
	//private Color defaultSkyboxColor;

	// Use this for initialization
	void Start () {
		defaultFog = RenderSettings.fog;
		defaultFogColor = RenderSettings.fogColor;
		defaultFogDensity = RenderSettings.fogDensity;
		//defaultSkyboxColor = RenderSettings.skybox.color;
		camera.backgroundColor = new Color(0.0f, 0.4f, 0.7f, 1.0f);
	}
	
	// Update is called once per frame
	void Update () {
		if(transform.position.y < water.transform.position.y)
		{
			RenderSettings.fog = false;
			RenderSettings.fogColor = new Color(0.0f, 0.1f, 0.3f, 0.6f);
			RenderSettings.fogDensity = 0.04f;
			//RenderSettings.skybox.color = new Color(0.0f, 0.1f, 0.3f, 0.6f);
		}
		else
		{
			RenderSettings.fog = defaultFog;
			RenderSettings.fogColor = defaultFogColor;
			RenderSettings.fogDensity = defaultFogDensity;
			//RenderSettings.skybox.color = defaultSkyboxColor;
		}
	}
}
