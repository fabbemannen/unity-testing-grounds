﻿using UnityEngine;
using System.Collections;

public class Spawn_Object : MonoBehaviour {

	public bool DeleteOnLMBRelease = false;

	private float fireRate = 0.05F;
	private float nextFire = 0.0F;
	public GameObject go;
	private float go_speed = 50000.0f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (Time.time > nextFire)
		{
			nextFire = Time.time + fireRate;
			GameObject projectile = Instantiate(go, new Vector3(transform.position.x, transform.position.y, transform.position.z), 
			                                    transform.rotation) as GameObject;
			projectile.rigidbody.AddForce(transform.parent.TransformDirection (Vector3.forward * go_speed * Time.deltaTime));
		}

		if(DeleteOnLMBRelease == true && !Input.GetButton("Fire1"))
		{
			Destroy(transform.parent.gameObject as Object);
		}
	}

	public void SetFireRate(float value)
	{
		fireRate = value;
	}

	public void SetSpeed(float value)
	{
		go_speed = value;
	}
}
