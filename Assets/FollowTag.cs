﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FollowTag : MonoBehaviour {

	public string target_tag = "Turret";
	public GameObject target_lookat;
	public bool LookAtOnly = true;
	public float minTime = 0.5F;
	public float maxTime = 0.5F;
	private float rateTime = 2.5f;
	private float nextTime = 0.0F;
	private GameObject[] targets;

	// Use this for initialization
	void Start () 
	{
		rateTime = Random.Range(minTime, maxTime);
		if(target_lookat == null)
		{
			target_lookat = GameObject.FindGameObjectWithTag("Player");
		}
	}
	
	// Update is called once per frame
	void Update () 
	{
		transform.LookAt(target_lookat.transform);
		if(Time.time > nextTime && !LookAtOnly)
		{
			nextTime = Time.time + rateTime;
			rateTime = Random.Range(minTime, maxTime);
			targets = GameObject.FindGameObjectsWithTag(target_tag);
			if(targets.Length > 0)
			{
				//transform.parent = targets[Random.Range(0, targets.Length)].transform; //<--for random
				//transform.position = targets[Random.Range(0, targets.Length)].transform.position;

				float dist = Vector3.Distance(target_lookat.transform.position, targets[0].transform.position); //<--for closest one
				int closest = 0;
				for(int i = 1; i < targets.Length; i++)
				{
					if(dist > Vector3.Distance(target_lookat.transform.position, targets[i].transform.position))
					{
						dist = Vector3.Distance(target_lookat.transform.position, targets[i].transform.position);
						closest = i;
					}
				}
				transform.parent = targets[closest].transform;

				transform.localPosition = new Vector3(0.0f, 0.0f, 0.0f);
				//transform.position = transform.parent.position;
			}
		}
	}
}
