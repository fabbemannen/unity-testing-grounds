﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Checkpoint_SAI : MonoBehaviour {

	public float cp_radian = 100.0f;
	public float areaX_min = -500.0f;
	public float areaX_max = 500.0f;
	public float areaY_min = -0.0f;
	public float areaY_max = 0.0f;
	public float areaZ_min = -500.0f;
	public float areaZ_max = 500.0f;
	public List<GameObject> Players = new List<GameObject>();

	// Use this for initialization
	void Start () {
		Vector3 newPos = new Vector3(Random.Range(areaX_min, areaX_max), Random.Range(areaY_min, areaY_max), Random.Range(areaZ_min, areaZ_max));
		gameObject.transform.position = newPos;
	}
	
	// Update is called once per frame
	void Update ()
	{
		for(int i = 0; i < Players.Count; i++)
		{
			if(Vector3.Distance(Players[i].transform.position, gameObject.transform.position) < cp_radian)
			{
				//Player hit the CP
				Debug.Log (string.Format("{0} hit the CP", Players[i].name));

				Vector3 newPos = new Vector3(Random.Range(areaX_min, areaX_max), Random.Range(areaY_min, areaY_max), Random.Range(areaZ_min, areaZ_max));
				gameObject.transform.position = newPos;
				break;
			}
		}
		//Debug.Log (string.Format("Distance to CP: {0}", Vector3.Distance(Players[0].transform.position, gameObject.transform.position)));
	}
}
