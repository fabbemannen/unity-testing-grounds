﻿using UnityEngine;
using System.Collections;

public class Controller_Ball : MonoBehaviour {

	public int speed = 10;
	public int sp_increase = 5;
	private int sp_forward = 0;
	private int sp_back = 0;
	private int sp_right = 0;
	private int sp_left = 0;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetAxis("Vertical") > 0)
		{
			rigidbody.AddForce(Vector3.forward * (speed + sp_forward) * Time.deltaTime);
			sp_forward += sp_increase;
			sp_back = 0;
		}
		else if(Input.GetAxis("Vertical") < 0)
		{
			rigidbody.AddForce(Vector3.forward * (-1) * (speed + sp_back) * Time.deltaTime);
			sp_back += sp_increase;
			sp_forward = 0;
		}
		else
		{
			if(sp_forward > 0)
			{
				sp_forward -= sp_increase;
			}

			if(sp_back > 0)
			{
				sp_back -= sp_increase;
			}
		}

		if(Input.GetAxis("Horizontal") > 0)
		{
			rigidbody.AddForce(Vector3.right * (speed + sp_right) * Time.deltaTime);
			sp_right += sp_increase;
			sp_left = 0;
		}
		else if(Input.GetAxis("Horizontal") < 0)
		{
			rigidbody.AddForce(Vector3.left * (speed + sp_left) * Time.deltaTime);
			sp_left += sp_increase;
			sp_right = 0;
		}
		else
		{
			if(sp_right > 0)
			{
				sp_right -= sp_increase;
			}
			
			if(sp_left > 0)
			{
				sp_left -= sp_increase;
			}
		}

		if(Input.GetAxis("Jump") > 0)
		{
			rigidbody.AddForce(Vector3.up * speed * Time.deltaTime);
		}


		//gameObject.transform.parent.transform.position = gameObject.transform.position;
	}
}
