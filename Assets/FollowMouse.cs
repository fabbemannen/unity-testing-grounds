﻿using UnityEngine;
using System.Collections;

public class FollowMouse : MonoBehaviour {
	
	public bool smooth = false;
	public bool hideMouse = false;

	// Use this for initialization
	void Start () {
		if(hideMouse)
		{
			Screen.showCursor = false;
		}
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.mousePresent)
		{


			if(smooth == false)
			{
				//gameObject.transform.position.Set(Input.mousePosition.x, Input.mousePosition.y, 1.0f);
				float distance = transform.position.z - Camera.main.transform.position.z;
				Vector3 targetPos = new Vector3(Input.mousePosition.x, Input.mousePosition.y, distance);
				targetPos = Camera.main.ScreenToWorldPoint(targetPos);
				gameObject.transform.position = targetPos;

				//Debug.Log (string.Format("Mouse x: {0}, y: {1}, z: {2}", Input.mousePosition.x, Input.mousePosition.y, Input.mousePosition.z));
			}
		}
	}
}
