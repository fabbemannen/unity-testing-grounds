﻿using UnityEngine;
using System.Collections;

public class AI_Flocking_Spawn : MonoBehaviour {
	public GameObject obj;
	public GameObject target;
	
	private GameObject[] leaders;
	private GameObject[] followers;
	private GameObject[] passives;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetMouseButtonDown(0))
		{
			GameObject clone = Instantiate(obj, new Vector3(0, 0, 0), transform.rotation) as GameObject;
			RotateTowards script = clone.GetComponent<RotateTowards>();
			script.target = target;
			AI_Flocking child_script = clone.GetComponentInChildren<AI_Flocking>();
			child_script.target = target;
		}
		if(Input.GetMouseButtonDown(1) || Input.GetMouseButtonDown(2))
		{
			leaders = GameObject.FindGameObjectsWithTag("AI_Flock_Leader");
			followers = GameObject.FindGameObjectsWithTag("AI_Flock_Follower");
			passives = GameObject.FindGameObjectsWithTag("AI_Flock_Passive");

			if(Input.GetMouseButtonDown(2))
			{
				if(leaders.Length > 0)
				{
					leaders[Random.Range(0, leaders.Length - 1)].GetComponent<Position_Random>().NewPos();
				}
			}
			else
			{
				if(leaders.Length > 0)
				{
					for(int i = 0; i < leaders.Length; i++)
					{
						leaders[i].GetComponent<Position_Random>().NewPos();
					}
				}
				if(followers.Length > 0)
				{
					for(int i = 0; i < followers.Length; i++)
					{
						followers[i].GetComponent<Position_Random>().NewPos();
					}
				}
				if(passives.Length > 0)
				{
					for(int i = 0; i < passives.Length; i++)
					{
						passives[i].GetComponent<Position_Random>().NewPos();
					}
				}
			}
		}
	}
}
