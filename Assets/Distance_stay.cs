﻿using UnityEngine;
using System.Collections;

public class Distance_stay : MonoBehaviour {

	public GameObject target;
	private GameObject org_target;
	public float distance_to_object = 30.0f;
	public float speed = 100.0f;

	public bool Random_Target = false;
	public string target_tag = "Turret";
	public float minTime = 0.5F;
	public float maxTime = 0.5F;
	private float rateTime = 2.5f;
	private float nextTime = 0.0F;
	private GameObject[] targets;

	// Use this for initialization
	void Start () 
	{
		rateTime = Random.Range(minTime, maxTime);
		org_target = target;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(Vector3.Distance(gameObject.transform.position, target.transform.position) > distance_to_object + 1 ||
		   Vector3.Distance(gameObject.transform.position, target.transform.position) < distance_to_object - 1)
		{
			gameObject.transform.LookAt(target.transform.position);
			gameObject.rigidbody.AddForce(gameObject.transform.TransformDirection (Vector3.forward * speed * Time.deltaTime));
		}
		else
		{
			gameObject.rigidbody.velocity = new Vector3(0.0f, 0.0f, 0.0f);
		}

		if(Time.time > nextTime && Random_Target == true)
		{
			nextTime = Time.time + rateTime;
			rateTime = Random.Range(minTime, maxTime);
			targets = GameObject.FindGameObjectsWithTag(target_tag);
			/*if(Vector3.Distance(gameObject.transform.position, org_target.transform.position) > 50.0f)
			{
				target = org_target;
			}
			else*/ 
			if(targets.Length > 0)
			{
				target = targets[Random.Range(0, targets.Length)];
			}
		}
	}
}
