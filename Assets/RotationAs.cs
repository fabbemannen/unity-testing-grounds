﻿using UnityEngine;
using System.Collections;

public class RotationAs : MonoBehaviour {

	public Transform target;
	public bool x = true;
	public bool y = true;
	public bool z = true;
	public bool w = true;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		Quaternion gtr = gameObject.transform.rotation;
		Quaternion ttr = target.rotation;
		if(x)
		{
			gtr.Set(ttr.x, gtr.y, gtr.z, gtr.w);
		}
		if(y)
		{
			gtr.Set(gtr.x, ttr.y, gtr.z, gtr.w);
		}
		if(z)
		{
			gtr.Set(gtr.x, gtr.y, ttr.z, gtr.w);
		}
		if(w)
		{
			gtr.Set(gtr.x, gtr.y, gtr.z, ttr.w);
		}
		
		Debug.Log (string.Format("gtr x: {0}, y: {1}, z: {2}", gtr.x, gtr.y, gtr.z));
		Debug.Log (string.Format("ttr x: {0}, y: {1}, z: {2}", ttr.x, ttr.y, ttr.z));
	}
}
