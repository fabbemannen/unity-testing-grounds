﻿using UnityEngine;
using System.Collections;

public class Thirdperson_Camera : MonoBehaviour {

	private Vector3 fp_position;
	public GameObject tp_go;

	// Use this for initialization
	void Start () {
		fp_position = transform.localPosition;
	}
	
	// Update is called once per frame
	void Update () {
		if(Variables.thirdperson == true)
		{
			transform.localPosition = tp_go.transform.localPosition;
			transform.localRotation = tp_go.transform.localRotation;
		}
		else
		{
			transform.localPosition = fp_position;
		}
	}
}
