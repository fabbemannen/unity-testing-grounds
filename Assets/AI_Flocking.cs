﻿using UnityEngine;
using System.Collections;

public class AI_Flocking : MonoBehaviour {
	public string tag = "AI_Flock_Circle";
	public GameObject target; //Who is the leader aiming for? Might be pointless.
	public GameObject leader; //Who's your leader?
	public float dist_to_leader = 0.1f;
	public float rotation_speed_to_leader = 1.0f;

	private RandomMovement rm;
	private Light cur_light;
	private TrailRenderer cur_trail;
	private Move cur_move;
	private GameObject[] followers;

	// Use this for initialization
	void Start () {
		cur_light = transform.parent.GetComponent<Light>();
		cur_trail = transform.parent.GetComponent<TrailRenderer>();
		cur_move = transform.parent.GetComponent<Move>();

		cur_move.maxVelocity = 3.0f;
	}
	
	// Update is called once per frame
	void Update () {
		if(transform.parent.tag == "AI_Flock_Passive" && rm) //Random movement disabled (!rm)
		{
			rm = transform.parent.gameObject.AddComponent<RandomMovement>();
		}
		else if(transform.parent.tag == "AI_Flock_Follower")
		{
			float distance = Vector3.Distance(transform.parent.position, leader.transform.position);

			float step = rotation_speed_to_leader * Time.deltaTime;
			
			if(distance < dist_to_leader)
			{
				cur_move.maxVelocity = 10.0f;
				//Debug.Log (string.Format("AI_Flocking - Distance to leader (1): {0}, {1}", distance, dist_to_leader));
				float distance1 = Vector3.Distance(target.transform.position, leader.transform.position);
				float distance2 = Vector3.Distance(target.transform.position, transform.parent.position);
				if(distance1 > distance2 + 0.7f)
				{
					transform.parent.rigidbody.velocity *= 0.8f;
					
					//Vector3 targetDir = (target.transform.position - leader.transform.position) * 0.1f;
					Vector3 targetDir = transform.parent.position - leader.transform.position;
					Vector3 newDir = Vector3.RotateTowards(transform.parent.forward, targetDir, step, 0.0F);
					Debug.DrawRay(transform.parent.position, newDir, Color.red);
					transform.parent.rotation = Quaternion.LookRotation(newDir);
				}
				else
				{
					transform.parent.rotation = leader.transform.rotation;
					transform.parent.rigidbody.velocity = leader.transform.rigidbody.velocity;
				}
			}
			else if(distance < dist_to_leader * 2)
			{
				/*
				//Debug.Log (string.Format("AI_Flocking - Distance to leader (2): {0}", distance));
				Vector3 targetDir = transform.parent.position - leader.transform.position;
				Vector3 newDir = Vector3.RotateTowards(transform.parent.forward, targetDir, step, 0.0F);
				Debug.DrawRay(transform.parent.position, newDir, Color.red);
				transform.parent.rotation = Quaternion.LookRotation(newDir);
				*/
				cur_move.maxVelocity *= 0.8f;
			}
			else
			{
				//Debug.Log (string.Format("AI_Flocking - Distance to leader (2): {0}", distance));
				Vector3 targetDir = transform.parent.position - leader.transform.position;
				Vector3 newDir = Vector3.RotateTowards(transform.parent.forward, targetDir, step, 0.0F);
				Debug.DrawRay(transform.parent.position, newDir, Color.red);
				transform.parent.rotation = Quaternion.LookRotation(newDir);
				cur_move.maxVelocity = 15.0f;
			}
		}
		else if(transform.parent.tag == "AI_Flock_Leader")
		{
			if(Vector3.Distance(transform.parent.position, target.transform.position) < 1.0f)
			{
				if(cur_move.rigidbody.velocity.magnitude < 0.05f)
				{
					cur_move.rigidbody.velocity *= 0.0f;
				}
				cur_move.rigidbody.velocity *= 0.5f;
			}
			else
			{
				bool LostOne = false;
				if(Time.deltaTime % 3 == 0)
				{
					followers = GameObject.FindGameObjectsWithTag("AI_Flock_Follower");

					for(int i = 0; i < followers.Length; i++)
					{
						AI_Flocking scr = followers[i].GetComponentInChildren<AI_Flocking>();
						if(scr.leader = gameObject)
						{
							if(Vector3.Distance(transform.parent.position, followers[i].transform.position) > dist_to_leader * 2)
							{
								LostOne = true;
								break;
							}
						}
					}
				}
				if(LostOne)
				{
					cur_move.rigidbody.velocity *= 0.8f;
				}
			}
		}
	}

	void OnTriggerEnter(Collider other)
	{
		//Debug.Log ("AI_Flocking: 0, " + other.transform.parent.tag);

		if(leader == null && other.tag == "Flock_Circle" && transform.parent.tag != "AI_Flock_Leader")
		{
			Debug.Log ("AI_Flocking: 1 - " + transform.parent.tag + "/" + other.transform.parent.tag);
			AI_Flocking other_script = other.GetComponent<AI_Flocking>();
			if(rm)
			{
				rm.enabled = false;
			}

			if(other.transform.parent.tag == "AI_Flock_Leader" || other.transform.parent.tag == "AI_Flock_Follower")
			{
				Debug.Log ("AI_Flocking: 2 - " + transform.parent.tag + "/" + other.transform.parent.tag);
				if(other_script.leader)
				{
					leader = other_script.leader;
				}
				else
				{
					leader = other.transform.parent.gameObject;
					other.transform.parent.tag = "AI_Flock_Leader"; //Needs testing
				}
				transform.parent.tag = "AI_Flock_Follower";
			}
			else //If they both meet at the same time, decide who will become the leader. Might work out without deciding, needs testing.
			{
				Debug.Log ("AI_Flocking: 3 - " + transform.parent.tag + "/" + other.transform.parent.tag);
				if(transform.position.x > other.transform.position.x) //Not fool-proof, but works for now
				{
					transform.parent.tag = "AI_Flock_Leader";

					other.transform.parent.tag = "AI_Flock_Follower";
					other_script.leader = transform.parent.gameObject;
				}
			}
		}

		if(leader)
		{
			transform.parent.renderer.material.color = leader.renderer.material.color;
			cur_move.maxVelocity = 30.0f;
		}
		else
		{
			cur_move.maxVelocity = 10.0f;
		}

		if(transform.parent.tag == "AI_Flock_Leader")
		{
			cur_light.enabled = true;
			cur_trail.enabled = true;
			//transform.parent.GetComponent<RandomColor>().disco_light = true;
		}
		else
		{
			cur_light.enabled = false;
			cur_trail.enabled = false;
			//transform.parent.GetComponent<RandomColor>().disco_light = false;
		}
	}
}
