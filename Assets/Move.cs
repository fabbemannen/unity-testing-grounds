﻿using UnityEngine;
using System.Collections;

public class Move : MonoBehaviour {

	//Add enum for different directions and timer for self-destruct
	public float speed = 10;
	public float maxVelocity = 8;
	public bool reverseDir = false;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 current;
		if(reverseDir)
		{
			current = Vector3.back;
		}
		else
		{
			current = Vector3.forward;
		}

		gameObject.rigidbody.AddForce(gameObject.transform.TransformDirection (current * speed * Time.deltaTime));
		//Debug.Log(rigidbody.velocity.sqrMagnitude);
		if(rigidbody.velocity.sqrMagnitude > maxVelocity)
		{
			rigidbody.velocity *= 0.9f;
		}
	}
}
