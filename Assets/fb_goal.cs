﻿using UnityEngine;
using System.Collections;

public class fb_goal : MonoBehaviour {

	public GameObject left;
	public GameObject right;
	public GameObject particle;
	public GameObject respawn;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

	}

	void OnTriggerEnter(Collider other)
	{
		if(other.tag != "Untagged" && other.tag != "Player")
		{
			other.transform.position = respawn.transform.position;
			GameObject clone_left = Instantiate(particle, left.transform.position, left.transform.rotation) as GameObject;
			GameObject clone_right = Instantiate(particle, right.transform.position, right.transform.rotation) as GameObject;
		}
	}
}
