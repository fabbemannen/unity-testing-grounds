﻿using UnityEngine;
using System.Collections;

public class RenderCamTexture : MonoBehaviour {

	public Camera camera;
	public RenderTexture rt;
	public bool rtonly = true;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		camera.targetTexture = rt;
		RenderTexture.active = rt; 
		camera.Render();
		//screenTexture.ReadPixels( new Rect(0, 0, width , height ), 0, 0 );
		//screenTexture.Apply(false);
		if(rtonly == false)
		{
			RenderTexture.active = null;
			camera.targetTexture = null;
		}
	}
}
