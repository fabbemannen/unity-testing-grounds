﻿using UnityEngine;
using System.Collections;

public class Switch : MonoBehaviour {
	public enum CustomList 
	{
		fp_to_tp, bouncy
	}
	public CustomList type;
	public Light light;
	public bool active = false;
	public bool value = false;

	// Use this for initialization
	void Start () {
		Variables.thirdperson = value;
	}
	
	// Update is called once per frame
	void Update () {
		if(active == true)
		{
			light.enabled = true;
		}
		else
		{
			light.enabled = false;
		}
	}

	void OnTriggerEnter(Collider other)
	{
		if(other.tag == "Projectile")
		{
			switch ((int)type)
			{
			case 0: //First person to third person
				if(active == true)
				{
					active = false;
					Variables.thirdperson = false;
				}
				else
				{
					active = true;
					Variables.thirdperson = true;
				}
				value = Variables.thirdperson;
				break;
			case 1:
				if(active == true)
				{
					active = false;
					Variables.player_bouncy = false;
				}
				else
				{
					active = true;
					Variables.player_bouncy = true;
				}
				value = Variables.player_bouncy;
				
				break;
			}
		}
	}
}
